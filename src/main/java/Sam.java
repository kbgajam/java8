import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Sam {

	public static void main(String[] args) {
		
		 System.out.println(triplets(8, Arrays.asList(1,2,3,4,5)) );
		 System.out.println("\t\t\t");
		 
		Performance p_2020_1 = new Performance(1,1,LocalDate.of(2020, 3, 1), 1);
		Performance p_2020_2 = new Performance(2, 2,LocalDate.of(2020, 3, 1), 2);
		Performance p_2020_3 = new Performance(3,3,LocalDate.of(2020, 3, 1), 3);
		
		Performance p_2019_1 = new Performance(4,1,LocalDate.of(2019, 3, 1), 1);
		Performance p_2019_2 = new Performance(5,2,LocalDate.of(2019, 3, 1), 2);
		Performance p_2019_3 = new Performance(6,3,LocalDate.of(2019, 3, 1), 3);
		
		Performance p_2018_1 = new Performance(7,1,LocalDate.of(2018, 3, 1), 1);
		Performance p_2018_2 = new Performance(8,2,LocalDate.of(2018, 3, 1), 2);
		Performance p_2018_3 = new Performance(9,3,LocalDate.of(2018, 3, 1), 3);
		
		Employee emp1 = new Employee("Krishna", 1, "admin", 40L, Arrays.asList(p_2020_1,p_2019_3, p_2018_2));
		Employee emp2 = new Employee("GB", 2, "tester",  20L,Arrays.asList(p_2020_2,p_2019_1, p_2018_3));
		Employee emp3 = new Employee("GB Krishna", 3, "admin", 30L, Arrays.asList(p_2020_3,p_2019_2, p_2018_1));
		
		List<Employee> dataList = new ArrayList<>(Arrays.asList(emp1,emp2,emp3));
		
		// Stream<Employee> finalList = dataList.stream().filter( emp -> emp.getPeformanceList().stream().filter( p -> p.getYear()==1) );
		/*
		 * List<Employee> finalList = new ArrayList<>(); dataList.stream().forEach(emp
		 * ->{ List<Performance> performList = emp.getPeformanceList().stream().filter(p
		 * -> p.getRanking() ==1).collect(Collectors.toList());
		 * if(!performList.isEmpty()) { emp.setPeformanceList(performList);
		 * finalList.add(emp); } });
		 * 
		 * finalList.stream().forEach(emp -> { System.out.println(emp.getName() + "\n");
		 * emp.getPeformanceList().stream().forEach(p ->{ System.out.println( "\t" +
		 * p.getYear() +" "+ p.getRanking()); }); } );
		 */
		
		//group by department
		Map<String,List<Employee>> map = dataList.stream().collect(Collectors.groupingBy(Employee::getDepartment));
		System.out.println(map);
		
		//average salary
		Double salary = dataList.stream().collect(Collectors.averagingDouble(Employee::getSalary));
		System.out.println("avg salary "+salary);
		
		//
		List<Employee> pempList = dataList.stream().filter(e -> e.getPeformanceList().stream().anyMatch(p -> p.getRanking() == 1)).collect(Collectors.toList());
		
		System.out.println(map);
		System.out.println("who got first rank");
		pempList.stream().forEach(e -> System.out.println(e.getName() +" "+e.getPeformanceList().size()));
		
		//sorting by salary
		System.out.println("sorting by salary");
		dataList.stream().sorted(Comparator.comparing(Employee::getSalary).reversed()).skip(1).toList().forEach(e -> System.out.println(e.getName()));
		
		
		//min salary
		System.out.println("min salary");
		Employee minSalaryEmployee = dataList.stream().min(Comparator.comparing(Employee::getSalary)).get();
		System.out.println("min salary emp --> "+ minSalaryEmployee);
		
		//avg of rankings performance of all employees
		Map avgList = dataList.stream().flatMap(e -> e.getPeformanceList().stream()).collect(Collectors.groupingBy(p -> p.getEmployeeId(), Collectors.averagingLong(Performance::getRanking)));
		System.out.println(avgList);
		
		//employee, number of ranks
		/*
		 * dataList.stream().collect(Collectors.map(e->e.getId(), Collectors.mapping(e
		 * -> e.getPeformanceList(),Collectors.toList() ) );
		 */
		
		
	}
	
	
	static long triplets(long t, List<Integer> d) {
        int temp=0;
        long count =0;
        for(int i=0; i< d.size() ; i++){
            temp = d.get(i);
            for(int j=i+1; j<d.size(); j++){
                int temp1 = temp + d.get(j);
                for(int k=j+1; k<d.size(); k++){
                    int temp2 = d.get(k);
                    if((temp1 + temp2) <=t){
                      count ++;  
                      System.out.println(d.get(i) + "+"+ d.get(j) + "+" + d.get(k));
                    }
                }
            }
        } 
        return count;   
    }

}

class Employee {
	String name;
	int id;
	Long salary;
	String department;
	
	List<Performance> peformanceList = new ArrayList<>();
	
	public Employee(String name, int id,String department, Long salary, List<Performance> peformanceList) {
		super();
		this.name = name;
		this.id = id;
		this.department = department;
		this.salary = salary;
		this.peformanceList = peformanceList;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Performance> getPeformanceList() {
		return peformanceList;
	}

	public void setPeformanceList(List<Performance> peformanceList) {
		this.peformanceList = peformanceList;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Long getSalary() {
		return salary;
	}

	public void setSalary(Long salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", id=" + id + ", salary=" + salary + ", department=" + department
				+ ", peformanceList=" + peformanceList + "]";
	}
	
}

class Performance {
	Integer performanceId;
	Integer employeeId;
	LocalDate year;
	int ranking;
	
	public Performance(Integer performanceId, Integer employeeId, LocalDate year, int ranking) {
		super();
		this.performanceId = performanceId;
		this.employeeId = employeeId;
		this.year = year;
		this.ranking = ranking;
	}
	
	public LocalDate getYear() {
		return year;
	}
	public void setYear(LocalDate year) {
		this.year = year;
	}
	public int getRanking() {
		return ranking;
	}
	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	public Integer getPerformanceId() {
		return performanceId;
	}

	public void setPerformanceId(Integer performanceId) {
		this.performanceId = performanceId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	
	
}
