import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Tricky1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str = "balakrishna ";
		String arry[] = str.split("");
		for(int i=arry.length-1 ; i >=0; i--) {
			System.out.print(arry[i]);
		}
		
		
		/*
		 * str.chars().forEach( c -> System.out.println((char)c));
		 * 
		 * System.out.println(" "); Stream.of(arry).forEach(System.out::println);
		 * 
		 * StringBuffer sb = new StringBuffer(str);
		 * 
		 * sb.reverse();
		 */
		
		
		Map map = str.chars().mapToObj(c -> (char)c).collect(Collectors.groupingBy(c -> c, Collectors.counting()));
		
		System.out.println(map);
		
		
		String str1 = "gbkrish";
		
		//vowels
		int ovewlCount = 0;
		int consonentCount = 0;
		
		List<Character> list = Arrays.asList('a','e','i','o','u') ;
		
		for(int i =0; i< str.length(); i++) {
			if(list.contains(str.charAt(i))) {
				ovewlCount++;
			}else if((int)str.charAt(i) >=97 && (int)str.charAt(i) <=122) {
				consonentCount++;
			}
		}
		
		System.out.println(ovewlCount +" " + consonentCount);
		
		//
		int[] array = {6, 3, 3, 6, 6,20, 6, 4, 4, 20};
		
		for(int i=0; i<array.length; i++) {
			for(int j=i+1 ; j<array.length; j++) {
				if(array[i] > array[j]) {
					int temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}
		
		for(int a : array) {
			System.out.println(a);
		}
		
		int sum = 0;
		Map<Integer, Integer> hmap = new HashMap<Integer,Integer>();
		for(int i=0; i<array.length ; i++) {
			if(i != array.length -1 && array[i] == array[i+1]) {
				sum += array[i];
			}else {
				sum += array[i];
				hmap.put(array[i], sum);
				sum = 0;
			}
		}
		
		hmap.forEach((k,v) -> {
			System.out.println(k +" " + v);
		});
		
		System.out.println("duplicate elements -->");
		//duplicate elements
		Arrays.stream(array).boxed().collect(Collectors.toSet()).forEach(System.out::println);
		
		System.out.println("----");
		Arrays.stream(array).boxed().collect(Collectors.partitioningBy(i -> i%2 ==0)).forEach((k,v) -> {
			v.forEach(f ->{
				System.out.println(f);
			});
			System.out.println("----");
		});
		System.out.println("----");
		
		
		Map<Character, Long> charCountMap = str.chars().mapToObj(c -> (char) c)
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
     
		System.out.println(charCountMap);
    
		//sorting
		List orderedList = IntStream.of(array).boxed().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
		System.out.println(orderedList);
		
		
		//Employee e1 = new Employee(str1, sum, orderedList)
    
	}

}


